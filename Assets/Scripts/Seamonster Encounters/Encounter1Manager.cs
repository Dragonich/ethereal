﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the first encounter the player has with the Seamonster.
/// </summary>
public class Encounter1Manager : MonoBehaviour
{
    private AudioManager audioManager;
    private EntitiesManager entitiesManager;

    [Tooltip("The distance the Seamonster should travel from one side of the submarine to the other during a swimming pass.")]
    public float SeamonsterPassWidth;

    [Tooltip("The number of swimming passes the Seamonster should make during this Encounter before attacking the submarine, assuming that the Seamonster is not attacked & scared off.")]
    public int SeamonsterEncounterPasses;

    /// <summary>
    /// Represents the number of swimmming passes the Seamonster
    /// has currently made during this Encounter.
    /// </summary>
    private int seamonsterPassesCounter;

    /// <summary>
    /// Represents whether the Seamonster's growl sound effect has
    /// played during the Seamonster's current swimming pass.
    /// </summary>
    private bool audioPlayedThisPass;

    /// <summary>
    /// The current state of this Encounter.
    /// </summary>
    private EncounterStates currentEncounterState;

    /// <summary>
    /// The possible states in which this Encounter can be.
    /// </summary>
    private enum EncounterStates
    {
        /// <summary>
        /// The state of this Encounter when it is not active.
        /// </summary>
        Inactive,

        /// <summary>
        /// The state of this Encounter when the submarine's movement is being stopped.
        /// </summary>
        TerminatingSubmarine,
        
        /// <summary>
        /// The state of this Encounter when the Seamonster is swimming rightward.
        /// </summary>
        SeamonsterRightward,
        
        /// <summary>
        /// The state of this Encounter when the Seamonster is swimming leftward.
        /// </summary>
        SeamonsterLeftward,
        
        /// <summary>
        /// The state of this Encounter when the Seamonster is swimming
        /// along the centre of the screen, to attack the submarine.
        /// </summary>
        SeamonsterAttacking,
        
        /// <summary>
        /// The state of this Encounter when the Seamonster has been attacked,
        /// and is swimming away to escape.
        /// </summary>
        SeamonsterFleeing,
    }

    public void Initialise(AudioManager inputAudioManager, EntitiesManager inputEntitiesManager)
    {
        Debug.Assert(inputAudioManager != null);
        Debug.Assert(inputEntitiesManager != null);

        audioManager = inputAudioManager;
        entitiesManager = inputEntitiesManager;

        AssertInspectorInputs();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(SeamonsterPassWidth > 0f);
        Debug.Assert(SeamonsterEncounterPasses > 0);
    }

    /// <summary>
    /// Activates this encounter.
    /// </summary>
    public void ActivateEncounter()
    {
        //Debug.Log("Activating Encounter 1.");

        Debug.Assert(audioManager != null);
        Debug.Assert(entitiesManager != null);
        Debug.Assert(currentEncounterState == EncounterStates.Inactive); // Assert that the encounter is not already active.

        audioManager.PlayLoopedSonarAudio();

        currentEncounterState = EncounterStates.TerminatingSubmarine;

        entitiesManager.EjectSubmarinePilotState(); // Switch the player to be in the Character state, if they are in the Pilot state.
        entitiesManager.SetTerminatingForcesActive(true); // Sets the submarine to experience linear drag but not gravity.
        entitiesManager.SetPilotStateLocked(true); // Set the Pilot terminal / state to be locked and therefore non-enterable.
    }

    private void Update()
    {
        UpdateCurrentState();
    }

    /// <summary>
    /// Updates the functionality related to the current state.
    /// </summary>
    private void UpdateCurrentState()
    {
        if (currentEncounterState != EncounterStates.Inactive) // This Encounter is currently inactive.
        {
            switch (currentEncounterState)
            {
                case EncounterStates.TerminatingSubmarine:

                    UpdateTerminatingSubmarineState();
                    break;

                case EncounterStates.SeamonsterRightward:

                    UpdateSeamonsterRightwardState();
                    break;

                case EncounterStates.SeamonsterLeftward:

                    UpdateSeamonsterLeftwardState();
                    break;

                case EncounterStates.SeamonsterFleeing:

                    UpdateSeamonsterFleeing();
                    break;

                case EncounterStates.SeamonsterAttacking:

                    UpdateSeamonsterAttacking();
                    break;

                default:

                    Debug.LogError(currentEncounterState);
                    break;
            }
        }
    }

    /// <summary>
    /// Updates the functionality related to the Terminating Submarine state.
    /// </summary>
    private void UpdateTerminatingSubmarineState()
    {
        Debug.Assert(entitiesManager != null);

        if(!entitiesManager.GetIsSubmarineMoving()) // The submarine has terminated its movement.
        {
            entitiesManager.SetSeamonsterActive(true); // Set the Seamonster object to be active.
            entitiesManager.ResetSeamonsterHitPoints();
            entitiesManager.ResetSeamonsterRightwardVelocity(true);

            currentEncounterState = EncounterStates.SeamonsterRightward;

            entitiesManager.SetSeamonsterPositionRelativeToSubmarine(-SeamonsterPassWidth, 4f); // Set the Seamonster to be offscreen to the left.
        }
    }

    /// <summary>
    /// Updates the functionality related to the Seamonster Rightward state.
    /// </summary>
    private void UpdateSeamonsterRightwardState()
    {
        Debug.Assert(audioManager != null);
        Debug.Assert(entitiesManager != null);

        Vector2 seamonsterRelativePosition = entitiesManager.GetSeamonsterPositionRelativeToSubmarine();

        if(Mathf.Abs(seamonsterRelativePosition.x) < 10f && !audioPlayedThisPass) // The Seamonster is close to the submarine, and the audio clip has not been played this pass.
        {
            audioManager.PlayGroanAudio();

            audioPlayedThisPass = true; // Mark the audio as having been played during this Seamonster pass.
        }

        if(seamonsterRelativePosition.x > SeamonsterPassWidth) // The Seamonster has completed its current pass.
        {
            seamonsterPassesCounter++; // Increment the number of passes completed.
            entitiesManager.FlipSeamonster();

            if (seamonsterPassesCounter > SeamonsterEncounterPasses) // The Seamonster has completed the maximum number of passes for this Encounter.
            {
                /*
                entitiesManager.SetSeamonsterRelativePositionY(0f);

                audioPlayedThisPass = false;
                currentEncounterState = EncounterStates.SeamonsterAttacking;
                */

                Debug.LogError("Seamonster completed its passes on its rightward state.");
            }
            else // The Seamonster has passes remaining for this Encounter.
            {
                entitiesManager.SetSeamonsterRelativePositionY(-16f); // Set the Seamonster to be below the submarine.

                audioPlayedThisPass = false;
                currentEncounterState = EncounterStates.SeamonsterLeftward;
            }
        }
    }

    /// <summary>
    /// Updates the functionality related to the Seamonster Leftward state.
    /// </summary>
    private void UpdateSeamonsterLeftwardState()
    {
        Debug.Assert(entitiesManager != null);

        Vector2 seamonsterRelativePosition = entitiesManager.GetSeamonsterPositionRelativeToSubmarine();

        if (Mathf.Abs(seamonsterRelativePosition.x) < 10f && !audioPlayedThisPass) // The Seamonster is close to the submarine, and the audio clip has not been played this pass.
        {
            audioManager.PlayGroanAudio();

            audioPlayedThisPass = true; // Mark the audio as having been played during this Seamonster pass.
        }

        if (seamonsterRelativePosition.x < -SeamonsterPassWidth) // The Seamonster has completed its current pass.
        {
            seamonsterPassesCounter++; // Increment the number of passes completed.
            entitiesManager.FlipSeamonster();

            if (seamonsterPassesCounter > SeamonsterEncounterPasses) // The Seamonster has completed the maximum number of passes for this Encounter.
            {
                entitiesManager.SetSeamonsterRelativePositionY(-5f); // Set the Seamonster to appear at the same vertical position as the submarine.

                audioPlayedThisPass = false;
                currentEncounterState = EncounterStates.SeamonsterAttacking;
            }
            else // The Seamonster has passes remaining for this Encounter.
            {
                entitiesManager.SetSeamonsterRelativePositionY(4f); // Set the Seamonster to be above the submarine.

                audioPlayedThisPass = false;
                currentEncounterState = EncounterStates.SeamonsterRightward;
            }
        }
    }

    /// <summary>
    /// Updates the functionality related to the Seamonster Attacking state.
    /// </summary>
    private void UpdateSeamonsterAttacking()
    {
        Debug.Assert(audioManager != null);
        Debug.Assert(entitiesManager != null);

        Vector2 seamonsterRelativePosition = entitiesManager.GetSeamonsterPositionRelativeToSubmarine();

        if (Mathf.Abs(seamonsterRelativePosition.x) < 30f && !audioPlayedThisPass) // The Seamonster is close to the submarine, and the audio clip has not been played during this attack.
        {
            audioManager.PlayGrowlAudio();

            audioPlayedThisPass = true; // Mark the audio as having been played during this Seamonster attack.
        }
    }

    /// <summary>
    /// Updates the functionality related to the Seamonster Fleeing state.
    /// </summary>
    private void UpdateSeamonsterFleeing()
    {
        Debug.Assert(entitiesManager != null);

        Vector2 seamonsterRelativePosition = entitiesManager.GetSeamonsterPositionRelativeToSubmarine();

        if(Mathf.Abs(seamonsterRelativePosition.y) > SeamonsterPassWidth / 2f) // The Seamonster has reached a sufficient vertical distance from the submarine.
        {
            EndEncounter();
        }
    }

    /// <summary>
    /// Triggers the Encounter to finish by entering its final stage.
    /// </summary>
    public void TriggerEncounterToFinish()
    {
        Debug.Assert(audioManager != null);
        Debug.Assert(entitiesManager != null);

        Vector2 seamonsterRelativePosition = entitiesManager.GetSeamonsterPositionRelativeToSubmarine();

        bool seamonsterAboveSubmaine = seamonsterRelativePosition.y > 0f;
        entitiesManager.AddSeamonsterVerticalForce(seamonsterAboveSubmaine); // Add vertical force to the Seamonster to push it away from the submarine.

        audioManager.PlayGrowlAudio();
        currentEncounterState = EncounterStates.SeamonsterFleeing;
    }

    /// <summary>
    /// Ends this Encounter, and returns the user to normal gameplay.
    /// </summary>
    private void EndEncounter()
    {
        Debug.Assert(audioManager != null);
        Debug.Assert(entitiesManager != null);

        currentEncounterState = EncounterStates.Inactive;

        audioManager.StopSonarAudio();

        entitiesManager.SetTerminatingForcesActive(false); // Sets the submarine to experience gravity but not linear drag.
        entitiesManager.SetPilotStateLocked(false); // Set the Pilot terminal / state to be unlocked and therefore enterable.

        entitiesManager.SetSeamonsterActive(false); // Set the Seamonster object to be not active.
    }
}