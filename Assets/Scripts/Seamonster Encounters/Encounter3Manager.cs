﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the third encounter the player has with the Seamonster.
/// </summary>
public class Encounter3Manager : MonoBehaviour
{
    private EntitiesManager entitiesManager;

    [Tooltip("The horizontal distance from the submarine at which the Seamonster should spawn.")]
    public float SeamonsterSpawnOffset;

    /// <summary>
    /// The current state of this Encounter.
    /// </summary>
    private EncounterStates currentEncounterState;

    /// <summary>
    /// The possible states in which this Encounter can be.
    /// </summary>
    private enum EncounterStates
    {
        /// <summary>
        /// The state of this Encounter when it is not active.
        /// </summary>
        Inactive,

        /// <summary>
        /// The state of this Encounter when the submarine's movement is being stopped.
        /// </summary>
        TerminatingSubmarine,

        /// <summary>
        /// The state of this Encounter when the Seamonster is swimming
        /// along the centre of the screen, to attack the submarine.
        /// </summary>
        SeamonsterAttacking,
    }

    public void Initialise(EntitiesManager inputEntitiesManager)
    {
        Debug.Assert(inputEntitiesManager != null);
        entitiesManager = inputEntitiesManager;

        AssertInspectorInputs();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(SeamonsterSpawnOffset > 0f);
    }

    /// <summary>
    /// Activates this encounter.
    /// </summary>
    public void ActivateEncounter()
    {
        //Debug.Log("Activating Encounter 3.");

        Debug.Assert(entitiesManager != null);
        Debug.Assert(currentEncounterState == EncounterStates.Inactive); // Assert that the encounter is not already active.

        entitiesManager.SetPilotStateLocked(true); // Disable the player from controlling the submarine.

        currentEncounterState = EncounterStates.TerminatingSubmarine;
        entitiesManager.SetTerminatingForcesActive(true); // Sets the submarine to experience linear drag but not gravity.
    }

    private void Update()
    {
        UpdateCurrentState();
    }

    /// <summary>
    /// Updates the functionality related to the current state.
    /// </summary>
    private void UpdateCurrentState()
    {
        if (currentEncounterState != EncounterStates.Inactive) // This Encounter is currently inactive.
        {
            switch (currentEncounterState)
            {
                case EncounterStates.TerminatingSubmarine:

                    UpdateTerminatingSubmarineState();
                    break;

                case EncounterStates.SeamonsterAttacking: break;

                default:

                    Debug.LogError(currentEncounterState);
                    break;
            }
        }
    }

    /// <summary>
    /// Updates the functionality related to the Terminating Submarine state.
    /// </summary>
    private void UpdateTerminatingSubmarineState()
    {
        Debug.Assert(entitiesManager != null);

        if (!entitiesManager.GetIsSubmarineMoving()) // The submarine has terminated its movement.
        {
            entitiesManager.SetSeamonsterActive(true); // Set the Seamonster object to be active.

            currentEncounterState = EncounterStates.SeamonsterAttacking;
            //Debug.Log("Set Seamonster to be attacking.");

            entitiesManager.SetSeamonsterHorizontal();
            entitiesManager.ResetSeamonsterRightwardVelocity(false); // Reset the Seamonster to its rightward velocity with its Encounter 3 speed.
            entitiesManager.SetSeamonsterPositionRelativeToSubmarine(-SeamonsterSpawnOffset, -6f); // Set the Seamonster to be directly leftward of the submarine.
        }
    }
}