﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Detects collisions of the Seamonster with the player & projectile objects,
/// and reports each of these to the Seamonster Manager.
/// </summary>
public class SeamonsterTrigger : MonoBehaviour
{
    private SeamonsterManager seamonsterManager;

    public void Initialise(SeamonsterManager inputSeamonsterManager)
    {
        Debug.Assert(inputSeamonsterManager != null);
        seamonsterManager = inputSeamonsterManager;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Assert(collision != null);

        Debug.Assert(seamonsterManager != null);

        if(collision.tag == "Player")
        {
            seamonsterManager.TriggerSubmarineAttacked(); // Register the collision with the Seamonster manager.
        }
        else if(collision.tag == "Projectile")
        {
            seamonsterManager.SeamonsterProjectileCollision(); // Register the collision with the Seamonster manager.

            Destroy(collision.gameObject);
        }
    }
}