﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The trigger which activates the given encounter.
/// </summary>
public class EncounterTrigger : MonoBehaviour
{
    [Tooltip("Whether this trigger should be visible during gameplay for development purposes.")]
    public bool ShowTriggerDuringGameplay;

    private EncountersManager encountersManager;

    [Tooltip("The encounter for which this is a trigger.")]
    public EncountersManager.Encounters Encounter;

    private SpriteRenderer spriteRenderer;

    public void Initialise(EncountersManager inputEncountersManager)
    {
        Debug.Assert(inputEncountersManager != null);
        encountersManager = inputEncountersManager;

        AssertInspectorInputs();

        CacheSubordinates();

        if(!ShowTriggerDuringGameplay)
        {
            spriteRenderer.enabled = false;
        }
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(Encounter != EncountersManager.Encounters.None);
    }

    private void CacheSubordinates()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        Debug.Assert(spriteRenderer != null); // Assert that the component was found.
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Assert(collision != null);
        Debug.Assert(encountersManager != null);

        if(collision.tag == "Player")
        {
            encountersManager.ActivateEncounter(Encounter);

            Destroy(gameObject);
        }
    }
}