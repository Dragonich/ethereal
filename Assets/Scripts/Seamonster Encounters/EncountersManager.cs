﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the various encounters in the game.
/// </summary>
public class EncountersManager : MonoBehaviour
{
    private AppManager appManager;

    /// <summary>
    /// The current encounter state which is active.
    /// </summary>
    private Encounters currentEncounter;

    /// <summary>
    /// The states which can be active in the game.
    /// </summary>
    public enum Encounters
    {
        None,
        Encounter1,
        Encounter2,
        Encounter3,
    }

    [Tooltip("The GameObject which holds the Encounter 1 Manager as a component.")]
    public GameObject GO_Encounter1Manager;

    [Tooltip("The GameObject which holds the Encounter 3 Manager as a component.")]
    public GameObject GO_Encounter3Manager;

    private Encounter1Manager encounter1Manager;
    private Encounter3Manager encounter3Manager;

    [Tooltip("The GameObject which holds the Encounter 1 Trigger as a component.")]
    public GameObject GO_Encounter1Trigger;

    [Tooltip("The GameObject which holds the Encounter 3 Trigger as a component.")]
    public GameObject GO_Encounter3Trigger;

    private EncounterTrigger encounter1Trigger;
    private EncounterTrigger encounter3Trigger;

    public void Initialise(AppManager inputAppManager)
    {
        Debug.Assert(inputAppManager != null);
        appManager = inputAppManager;

        AssertInspectorInputs();

        CacheSubordinates();

        InitialiseSubordinates();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_Encounter1Manager != null);
        Debug.Assert(GO_Encounter3Manager != null);

        Debug.Assert(GO_Encounter1Trigger != null);
        Debug.Assert(GO_Encounter3Trigger != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_Encounter1Manager != null);
        Debug.Assert(GO_Encounter3Manager != null);
        Debug.Assert(GO_Encounter1Trigger != null);
        Debug.Assert(GO_Encounter3Trigger != null);

        encounter1Manager = GO_Encounter1Manager.GetComponent<Encounter1Manager>();
        Debug.Assert(encounter1Manager != null); // Assert that the component was found.

        encounter3Manager = GO_Encounter3Manager.GetComponent<Encounter3Manager>();
        Debug.Assert(encounter3Manager != null); // Assert that the component was found.

        encounter1Trigger = GO_Encounter1Trigger.GetComponent<EncounterTrigger>();
        Debug.Assert(encounter1Trigger != null); // Assert that the component was found.

        encounter3Trigger = GO_Encounter3Trigger.GetComponent<EncounterTrigger>();
        Debug.Assert(encounter3Trigger != null); // Assert that the component was found.
    }

    private void InitialiseSubordinates()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.AudioManager != null);
        Debug.Assert(appManager.EntitiesManager != null);
        Debug.Assert(encounter1Manager != null);
        Debug.Assert(encounter3Manager != null);
        Debug.Assert(encounter1Trigger!= null);
        Debug.Assert(encounter3Trigger!= null);

        encounter1Manager.Initialise(appManager.AudioManager, appManager.EntitiesManager);
        encounter3Manager.Initialise(appManager.EntitiesManager);

        encounter1Trigger.Initialise(this);
        encounter3Trigger.Initialise(this);
    }

    /// <summary>
    /// Activates the given encounter.
    /// </summary>
    /// <param name="encounterToActivate">The encounter which should be activated.</param>
    public void ActivateEncounter(Encounters encounterToActivate)
    {
        Debug.Assert(encounterToActivate != Encounters.None); // Assert that None is not being activated.
        Debug.Assert(encounterToActivate != currentEncounter); // Assert that the encounter being activated is not the current encounter.

        Debug.Assert(encounter1Manager != null);
        Debug.Assert(encounter3Manager != null);
        Debug.Assert(currentEncounter == Encounters.None, currentEncounter); // Assert that no encounter is currently active.

        switch(encounterToActivate)
        {
            case Encounters.Encounter1:

                currentEncounter = Encounters.Encounter1;
                encounter1Manager.ActivateEncounter();
                break;

            case Encounters.Encounter3:

                currentEncounter = Encounters.Encounter3;
                encounter3Manager.ActivateEncounter();
                break;

            default:

                Debug.LogError(encounterToActivate);
                break;
        }
    }

    /// <summary>
    /// Triggers the current encounter to finish.
    /// </summary>
    public void TriggerCurrentEncounterToFinish()
    {
        Debug.Assert(encounter1Manager != null);
        Debug.Assert(currentEncounter != Encounters.None); // Assert that an Encounter is active.

        switch(currentEncounter)
        {
            case Encounters.Encounter1:

                encounter1Manager.TriggerEncounterToFinish();
                break;

            case Encounters.Encounter3:
                break;

            default:

                Debug.LogError(currentEncounter);
                break;
        }

        currentEncounter = Encounters.None;
    }

    /// <summary>
    /// Returns the current encounter, including None.
    /// </summary>
    public Encounters GetCurrentEncounter()
    {
        return currentEncounter;
    }
}