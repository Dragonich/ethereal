﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the various terminals of the submarine.
/// </summary>
public class SubmarineTerminalsManager : MonoBehaviour
{
    public float TerminalActivationDistance;

    public GameObject PilotTerminal;
    public GameObject LightsTerminal;
    public GameObject WeaponsTerminal;

    /// <summary>
    /// The terminals available on the submarine, including None.
    /// </summary>
    public enum Terminals
    {
        None,
        Pilot,
        Lights,
        Weapons,
    }

    public void Initialise()
    {
        AssertInspectorInputs();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(TerminalActivationDistance > 0f);

        Debug.Assert(PilotTerminal != null);
        Debug.Assert(LightsTerminal != null);
        Debug.Assert(WeaponsTerminal != null);
    }

    /// <summary>
    /// Returns the enum value of the closest valid terminal.
    /// If no valid terminal is found, returns the enum value of None.
    /// </summary>
    /// <param name="characterPosition">The position of the character object to be compared against terminal object positions.</param>
    public Terminals GetClosesetValidTerminal(Vector2 characterPosition)
    {
        Debug.Assert(PilotTerminal != null);
        Debug.Assert(LightsTerminal != null);
        Debug.Assert(WeaponsTerminal != null);

        float distanceToPilotTerminal = Vector2.Distance(PilotTerminal.transform.position, characterPosition);
        float distanceToLightsTerminal = Vector2.Distance(LightsTerminal.transform.position, characterPosition);
        float distanceToWeaponsTerminal = Vector2.Distance(WeaponsTerminal.transform.position, characterPosition);

        //Debug.Log("Distance To Pilot Terminal: " + distanceToPilotTerminal + "\nDistance To Lights Terminal: " + distanceToLightsTerminal + "\nDistance To Weapons Terminal: " + distanceToWeaponsTerminal);

        if(distanceToPilotTerminal < TerminalActivationDistance || distanceToLightsTerminal < TerminalActivationDistance || distanceToWeaponsTerminal < TerminalActivationDistance) // A terminal is within activation distance.
        {
            if (distanceToPilotTerminal < distanceToLightsTerminal && distanceToPilotTerminal < distanceToWeaponsTerminal) // The closest terminal is the Pilot Terminal.
            {
                return Terminals.Pilot; // Return the Pilot terminal as the closest valid terminal.
            }
            else // The closest terminal is not the Pilot terminal.
            {
                if (distanceToLightsTerminal < distanceToPilotTerminal && distanceToLightsTerminal < distanceToWeaponsTerminal) // The closest terminal is the Lights Terminal.
                {
                    return Terminals.Lights; // Return the Lights terminal as the closest valid terminal.
                }
                else // The closest terminal is the Weapons Terminal.
                {
                    return Terminals.Weapons; // Return the Weapons terminal as the closest valid terminal.
                }
            }
        }
        else // No terminal is within activation distance.
        {
            return Terminals.None; // Return None, as no terminal is within activation range.
        }
    }
}