﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the various entities in the game.
/// </summary>
public class EntitiesManager : MonoBehaviour
{
    private AppManager appManager;

    [Tooltip("The GameObject which holds the Character Manager as a component.")]
    public GameObject GO_CharacterManager;
    private CharacterManager characterManager;

    [Tooltip("The GameObject which holds the Submarine Manager as a component.")]
    public GameObject GO_SubmarineManager;
    private SubmarineManager submarineManager;

    [Tooltip("The GameObject which holds the Seamonster Manager as a component.")]
    public GameObject GO_SeamonsterManager;
    private SeamonsterManager seamonsterManager;

    public void Initialise(AppManager inputAppManager)
    {
        Debug.Assert(inputAppManager != null);
        appManager = inputAppManager;

        AssertInspectorInputs();

        CacheSubordinates();

        InitialiseSubordinates();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_CharacterManager != null);
        Debug.Assert(GO_SubmarineManager != null);
        Debug.Assert(GO_SeamonsterManager != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_CharacterManager != null);
        Debug.Assert(GO_SubmarineManager != null);
        Debug.Assert(GO_SeamonsterManager != null);

        characterManager = GO_CharacterManager.GetComponent<CharacterManager>();
        Debug.Assert(characterManager != null); // Assert that the component was found.

        submarineManager = GO_SubmarineManager.GetComponent<SubmarineManager>();
        Debug.Assert(submarineManager != null); // Assert that the component was found.

        seamonsterManager = GO_SeamonsterManager.GetComponent<SeamonsterManager>();
        Debug.Assert(seamonsterManager != null); // Assert that the component was found.
    }

    private void InitialiseSubordinates()
    {
        Debug.Assert(characterManager != null);
        Debug.Assert(submarineManager != null);
        Debug.Assert(seamonsterManager != null);

        characterManager.Initialise();

        submarineManager.Initialise();

        seamonsterManager.Initialise(appManager.StateManager, appManager.EncountersManager);
    }

    /// <summary>
    /// Sets the submarine's exterior panel and turret visible
    /// depending on whether the submarine's interior should be visible.
    /// </summary>
    /// <param name="newInteriorVisible">Whether the submarine's interior should be visible.</param>
    public void SetSubmarineInteriorVisible(bool newInteriorVisible)
    {
        Debug.Assert(submarineManager != null);

        submarineManager.SetSubmarineInteriorVisible(newInteriorVisible);
    }

    /// <summary>
    /// Moves the character object horizontally.
    /// </summary>
    /// <param name="moveRightward">Whether the character should be moved rightward.</param>
    public void MoveCharacter(bool moveRightward)
    {
        Debug.Assert(characterManager != null);

        characterManager.MoveCharacter(moveRightward);
    }

    /// <summary>
    /// If the Pilot terminal is not locked, applies force to
    /// the submarine's Rigidbody to move it in the given direction.
    /// </summary>
    /// <param name="movementDirection">The direction in which the submarine should be moved.</param>
    public void MoveSubmarine(SubmarineMovementManager.MovementDirections movementDirection)
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(submarineManager != null);

        if (!appManager.StateManager.PilotTerminalLocked) // The Pilot terminal is not locked, and the player can move the submarine.
        {
            submarineManager.MoveSubmarine(movementDirection);
        }
    }

    /// <summary>
    /// Returns the enum value of the closest valid terminal.
    /// If no valid terminal is found, returns the enum value of None.
    /// </summary>
    /// <param name="characterPosition">The position of the character object to be compared against terminal object positions.</param>
    public SubmarineTerminalsManager.Terminals GetClosestValidTerminal()
    {
        Debug.Assert(submarineManager != null);
        Debug.Assert(characterManager != null);

        return submarineManager.GetClosestValidTerminal(characterManager.GetCharacterPosition());
    }

    /// <summary>
    /// Rotates the submarine's light object.
    /// </summary>
    /// <param name="rotateClockwise">Whether the light object should be rotated clockwise.</param>
    public void RotateSubmarineLight(bool rotateClockwise)
    {
        Debug.Assert(submarineManager != null);

        submarineManager.RotateLight(rotateClockwise);
    }

    /// <summary>
    /// Rotates the submarine's turret object.
    /// </summary>
    /// <param name="rotateClockwise">Whether the turret object should be rotated clockwise.</param>
    public void RotateSubmarineTurret(bool rotateClockwise)
    {
        Debug.Assert(submarineManager != null);

        submarineManager.RotateTurret(rotateClockwise);
    }

    /// <summary>
    /// Instantiates a new projectile object and fires it from the submarine turret.
    /// </summary>
    public void FireTurretProjectile()
    {
        Debug.Assert(submarineManager != null);

        submarineManager.FireProjectile();
    }

    /// <summary>
    /// Sets the position of the Seamonster relative to the position of the submarine.
    /// </summary>
    /// <param name="newRelativePositionX">The X position offset from the submarine X position.</param>
    /// <param name="newRelativePositionY">The Y position offset from the submarine Y position.</param>
    public void SetSeamonsterPositionRelativeToSubmarine(float newRelativePositionX, float newRelativePositionY)
    {
        Debug.Assert(submarineManager != null);
        Debug.Assert(seamonsterManager != null);

        Vector2 submarinePosition = submarineManager.GetPosition();

        seamonsterManager.SetPosition(submarinePosition.x + newRelativePositionX, submarinePosition.y + newRelativePositionY);
    }

    /// <summary>
    /// Sets the Y position of the Seamonster relative to the position of the submarine,
    /// where the X position of the X position of the Seamonster is left at its current value.
    /// </summary>
    /// <param name="newRelativePositionY">The Y position offset from the submarine Y position.</param>
    public void SetSeamonsterRelativePositionY(float newRelativePositionY)
    {
        Debug.Assert(submarineManager != null);
        Debug.Assert(seamonsterManager != null);

        Vector2 submarinePosition = submarineManager.GetPosition();

        seamonsterManager.SetPositionY(submarinePosition.y + newRelativePositionY);
    }

    /// <summary>
    /// Returns the position of the Seamonster object, relative to the submarine object.
    /// </summary>
    public Vector2 GetSeamonsterPositionRelativeToSubmarine()
    {
        Debug.Assert(submarineManager != null);
        Debug.Assert(seamonsterManager != null);

        Vector2 submarinePosition = submarineManager.GetPosition();
        Vector2 seamonsterPosition = seamonsterManager.GetPosition();

        Vector2 relativePosition = Vector2.zero;
        relativePosition.x = seamonsterPosition.x - submarinePosition.x;
        relativePosition.y = seamonsterPosition.y - submarinePosition.y;

        return relativePosition;
    }

    /// <summary>
    /// Flips the Seamonster's horizontal rotation & velocity.
    /// </summary>
    public void FlipSeamonster()
    {
        Debug.Assert(seamonsterManager != null);

        seamonsterManager.FlipSeamonster();
    }

    /// <summary>
    /// Returns whether the magnitude of the submarine's velocity is above 0.1f.
    /// </summary>
    public bool GetIsSubmarineMoving()
    {
        Debug.Assert(submarineManager != null);

        return submarineManager.GetIsMoving();
    }

    /// <summary>
    /// Sets whether the submarine will experience linear drag and gravity.
    /// 
    /// If terminating forces are active, the submarine will experience linear drag,
    /// but not gravity. If the forces are not active, the submarine will experience
    /// gravity, but not linear drag.
    /// 
    /// This is used to stop the submarine's movement at the start of an Encounter,
    /// and to enable its movement again at the end of the Encounter.
    /// </summary>
    /// <param name="newTerminatingForcesActive">The new status of whether the submarine should experience linear drag but not gravity.</param>
    public void SetTerminatingForcesActive(bool newTerminatingForcesActive)
    {
        Debug.Assert(submarineManager != null);

        submarineManager.SetTerminatingForcesActive(newTerminatingForcesActive);
    }

    /// <summary>
    /// If the player is in the Pilot state, switches the player to be in the Character state.
    /// </summary>
    public void EjectSubmarinePilotState()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);

        appManager.StateManager.EjectPilotState();
    }

    /// <summary>
    /// Sets whether the Pilot terminal / state is locked and therefore cannot be entered.
    /// </summary>
    /// <param name="newLocked">The new status of whether the Pilot state is locked.</param>
    public void SetPilotStateLocked(bool newLocked)
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);

        appManager.StateManager.SetPilotStateLocked(newLocked);
    }

    /// <summary>
    /// Sets the Seamonster object's active status to the given boolean.
    /// </summary>
    /// <param name="newActive">Whether the Seamonster object should be active.</param>
    public void SetSeamonsterActive(bool newActive)
    {
        Debug.Assert(seamonsterManager != null);

        seamonsterManager.SetActive(newActive);
    }

    /// <summary>
    /// Resets the Seamonster's velocity to be moving right.
    /// </summary>
    /// <param name="encounter1">Whether this is the first encounter, which affects the Seamonster's speed.</param>
    public void ResetSeamonsterRightwardVelocity(bool encounter1)
    {
        Debug.Assert(seamonsterManager != null);

        seamonsterManager.ResetToRightwardVelocity(encounter1);
    }

    /// <summary>
    /// Adds vertical force to the Seamonster object's Rigidbody component.
    /// </summary>
    /// <param name="upwardForce">Whether the added force should push the Seamonster upward.</param>
    public void AddSeamonsterVerticalForce(bool upwardForce)
    {
        Debug.Assert(seamonsterManager != null);

        seamonsterManager.AddVerticalForce(upwardForce);
    }

    /// <summary>
    /// Resets the Seamonster's current hit-points to the maximum.
    /// </summary>
    public void ResetSeamonsterHitPoints()
    {
        Debug.Assert(seamonsterManager != null);

        seamonsterManager.ResetHitPoints();
    }

    /// <summary>
    /// Sets the rotation of the Seamonster to be horizontal.
    /// </summary>
    public void SetSeamonsterHorizontal()
    {
        Debug.Assert(seamonsterManager != null);

        seamonsterManager.SetHorizontal();
    }

    /// <summary>
    /// Sets the Idle Sprite to be active,
    /// and sets the Walk Sprite to be inactive.
    /// </summary>
    public void ActivateCharacterIdleSprite()
    {
        Debug.Assert(characterManager != null);

        characterManager.ActivateIdleSprite();
    }
}