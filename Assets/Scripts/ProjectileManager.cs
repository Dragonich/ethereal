﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages each projectile fired from the submarine's turret.
/// </summary>
public class ProjectileManager : MonoBehaviour
{
    [Tooltip("The seconds for which a projectile should be alive before destroy itself.")]
    public float LifetimeSeconds;

    private void Start()
    {
        Debug.Assert(LifetimeSeconds > 0f);

        Destroy(gameObject, LifetimeSeconds); // Set this object to delete itself after the set number of seconds.
    }
}