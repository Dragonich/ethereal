﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the game's camera.
/// </summary>
public class CameraManager : MonoBehaviour
{
    [Tooltip("The camera GameObject.")]
    public GameObject GO_Camera;

    /// <summary>
    /// Cached reference to the Camera component of the camera GameObject.
    /// </summary>
    private Camera CameraComponent;

    [Tooltip("The target orthographic size of the camera when focused on the character.")]
    public float CharacterOrthoSize;

    [Tooltip("The target orthographic size of the camera when focused on the submarine.")]
    public float SubmarineOrthoSize;

    [Tooltip("The rate at which the camera's orthographic size should transition between target values.")]
    public float OrthoSizeTransitionSpeed;

    /// <summary>
    /// The current state of the camera's orthographic size.
    /// </summary>
    private OrthoSizeStates orthoSizeState;

    /// <summary>
    /// The possible states of the camera's orthographic size.
    /// </summary>
    private enum OrthoSizeStates
    {
        /// <summary>
        /// The state when the camera's orthographic size
        /// is stationary on the character.
        /// </summary>
        Character,

        /// <summary>
        /// The state when the camera's orthographic size
        /// is stationary on the submarine.
        /// </summary>
        Submarine,

        /// <summary>
        /// The state when the camera's orthographic size
        /// is transitioning from being focused on the submarine,
        /// to being focused on the character.
        /// </summary>
        TransitioningToCharacter,

        /// <summary>
        /// The state when the camera's orthographic size
        /// is transitioning from being focused on the character,
        /// to being focused on the character.
        /// </summary>
        TransitioningToSubmarine,
    }

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();

        CameraComponent.orthographicSize = CharacterOrthoSize;
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_Camera != null);
        Debug.Assert(OrthoSizeTransitionSpeed > 0f);

        Debug.Assert(CharacterOrthoSize > 0f);
        Debug.Assert(SubmarineOrthoSize > 0f);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_Camera != null);

        CameraComponent = GO_Camera.GetComponent<Camera>();
        Debug.Assert(CameraComponent != null); // Assert that the component was found.
    }

    private void Update()
    {
        ApplyOrthoSizeTransition();
    }

    /// <summary>
    /// Modifies the camera's orthographic size, depending on its current state.
    /// </summary>
    private void ApplyOrthoSizeTransition()
    {
        Debug.Assert(CameraComponent != null);

        switch(orthoSizeState)
        {
            case OrthoSizeStates.TransitioningToCharacter: // The camera's orthographic size is transitioning to be focused on the character.

                CameraComponent.orthographicSize -= OrthoSizeTransitionSpeed; // Modify the camera's orthographic size.

                if (Mathf.Approximately(CameraComponent.orthographicSize, CharacterOrthoSize)) // The orthographic size has reached the target value.
                {
                    CameraComponent.orthographicSize = CharacterOrthoSize;

                    orthoSizeState = OrthoSizeStates.Character; // Set orthographic size's state to be stationary on the character.
                }
                break;

            case OrthoSizeStates.TransitioningToSubmarine: // The camera's orthographic size is transitioning to be focused on the submarine.

                CameraComponent.orthographicSize += OrthoSizeTransitionSpeed; // Modify the camera's orthographic size.

                if (Mathf.Approximately(CameraComponent.orthographicSize, SubmarineOrthoSize)) // The orthographic size has reached the target value.
                {
                    CameraComponent.orthographicSize = SubmarineOrthoSize;

                    orthoSizeState = OrthoSizeStates.Submarine; // Set the orthographic size's state to be stationary on the submarine.
                }
                break;
        }
    }

    /// <summary>
    /// Sets the current camera orthographic state to be either transitioning
    /// to be focused on the submarine, or to be transitioning to be focused
    /// on the character, depending on the given boolean.
    /// </summary>
    /// <param name="toSubmarine">Whether the transition should be to focus on the submarine, as opposed to the character.</param>
    public void ActivateOrthoSizeTransition(bool toSubmarine)
    {
        orthoSizeState = toSubmarine ? OrthoSizeStates.TransitioningToSubmarine : OrthoSizeStates.TransitioningToCharacter;
    }
}