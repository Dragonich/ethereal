﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the various states in the game.
/// </summary>
public class StateManager : MonoBehaviour
{
    private AppManager appManager;

    /// <summary>
    /// Represents whether the Pilot terminal is locked and therefore cannot be entered.
    /// </summary>
    public bool PilotTerminalLocked { get; private set; }

    /// <summary>
    /// The current state of the user.
    /// </summary>
    public States CurrentState { get; private set; }

    /// <summary>
    /// The states in which the user can be.
    /// </summary>
    public enum States
    {
        /// <summary>
        /// The state in which the user is using menu elements above the game.
        /// </summary>
        Menu,

        /// <summary>
        /// The state in which the user is controlling the character object.
        /// </summary>
        Character,

        /// <summary>
        /// The state in which the user is controlling the submarine's movement.
        /// </summary>
        Pilot,

        /// <summary>
        /// The state in which the user is controlling the submarine's lights.
        /// </summary>
        Lights,

        /// <summary>
        /// The state in which the user is controlling the submarine's weapons.
        /// </summary>
        Weapons,
    }

    public void Initialise(AppManager inputAppManager)
    {
        Debug.Assert(inputAppManager != null);
        appManager = inputAppManager;

        CurrentState = States.Character;
    }

    /// <summary>
    /// Activates the appropriate state depending on the context.
    /// 
    /// If the player is entering a submarine terminal state, then the
    /// nearest terminal will be determined and entered, or have no effect
    /// if there is no terminal within entering range.
    /// 
    /// If the player is not entering a submarine terminal state, then the
    /// player will be switched to the Character state.
    /// </summary>
    /// <param name="enteringSubmarineState">Represents whether the player is entering a submarine terminal state.</param>
    public void ActivateState(bool enteringSubmarineState)
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        if(enteringSubmarineState) // The player is entering a submarine terminal state.
        {
            SubmarineTerminalsManager.Terminals closestValidTerminal = appManager.EntitiesManager.GetClosestValidTerminal();

            switch(closestValidTerminal)
            {
                case SubmarineTerminalsManager.Terminals.Pilot:

                    if(!PilotTerminalLocked) // The pilot terminal is currently enterable.
                    {
                        ActivatePilotState();
                    }
                    break;

                case SubmarineTerminalsManager.Terminals.Weapons:

                    ActivateWeaponsState();
                    break;

                case SubmarineTerminalsManager.Terminals.Lights:

                    ActivateLightsState();
                    break;

                case SubmarineTerminalsManager.Terminals.None: break; // No terminal is within entering range.

                default:

                    Debug.LogError(closestValidTerminal);
                    break;
            }
        }
        else // The player is not entering a submarine terminal state.
        {
            ActivateCharacterState();
        }
    }

    /// <summary>
    /// If the player is in the Pilot state, switches the player to be in the Character state.
    /// </summary>
    public void EjectPilotState()
    {
        if(CurrentState == States.Pilot) // The player is in the Pilot state.
        {
            ActivateState(false); // Switch the player to the Character state.
        }
    }

    /// <summary>
    /// Sets whether the Pilot terminal / state is locked and therefore cannot be entered.
    /// </summary>
    /// <param name="newLocked">The new status of whether the Pilot state is locked.</param>
    public void SetPilotStateLocked(bool newLocked)
    {
        PilotTerminalLocked = newLocked;
    }

    /// <summary>
    /// Activates the state where the user is controlling menu elements.
    /// </summary>
    private void ActivateMenuState()
    {
        //Debug.Log("Activating menu state.");
    }

    /// <summary>
    /// Activates the state where the user is controlling the character.
    /// </summary>
    private void ActivateCharacterState()
    {
        //Debug.Log("Activating character state.");

        Debug.Assert(appManager != null);
        Debug.Assert(appManager.CameraManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        CurrentState = States.Character;

        appManager.EntitiesManager.SetSubmarineInteriorVisible(true); // Set the interior of the submarine to be visible.

        appManager.CameraManager.ActivateOrthoSizeTransition(false); // Activate the camera's orthographic size to transition to focus on the character.
    }

    /// <summary>
    /// Activates the state where the user is controlling the submarine's movement.
    /// </summary>
    private void ActivatePilotState()
    {
        //Debug.Log("Activating pilot state.");

        Debug.Assert(appManager != null);
        Debug.Assert(appManager.CameraManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        CurrentState = States.Pilot;

        appManager.EntitiesManager.SetSubmarineInteriorVisible(false); // Set the exterior of the submarine to be visible.

        appManager.CameraManager.ActivateOrthoSizeTransition(true); // Activate the camera's orthographic size to transition to focus on the submarine.
    }

    /// <summary>
    /// Activates the state where the user is controlling the submarine's light.
    /// </summary>
    private void ActivateLightsState()
    {
        //Debug.Log("Activating lights state.");

        Debug.Assert(appManager != null);
        Debug.Assert(appManager.CameraManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        CurrentState = States.Lights;

        appManager.EntitiesManager.SetSubmarineInteriorVisible(false); // Set the exterior of the submarine to be visible.

        appManager.CameraManager.ActivateOrthoSizeTransition(true); // Activate the camera's orthographic size to transition to focus on the submarine.
    }

    /// <summary>
    /// Activates the state where the user is controlling the submarine's weapons.
    /// </summary>
    private void ActivateWeaponsState()
    {
        //Debug.Log("Activating weapons state.");

        Debug.Assert(appManager != null);
        Debug.Assert(appManager.CameraManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        CurrentState = States.Weapons;

        appManager.EntitiesManager.SetSubmarineInteriorVisible(false); // Set the exterior of the submarine to be visible.

        appManager.CameraManager.ActivateOrthoSizeTransition(true); // Activate the camera's orthographic size to transition to focus on the submarine.
    }

    /// <summary>
    /// Triggers the game's fail-state, stopping all audio
    /// and presenting the user with the fail menu.
    /// </summary>
    public void TriggerFailState()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.UIManager != null);
        Debug.Assert(appManager.AudioManager != null);

        Time.timeScale = 0f;

        appManager.AudioManager.StopMusicAudio();
        appManager.AudioManager.StopSonarAudio();
        appManager.AudioManager.StopUnderwaterAudio();

        appManager.AudioManager.PlayFailStateAudio();
        appManager.UIManager.ActivateFailStateMenu();
    }

    /// <summary>
    /// Triggers the game's end-state, stopping all audio
    /// and presenting the user with the end menu.
    /// </summary>
    public void TriggerGameEnd()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.UIManager != null);
        Debug.Assert(appManager.AudioManager != null);

        Time.timeScale = 0f;

        appManager.AudioManager.StopMusicAudio();

        appManager.AudioManager.PlayFailStateAudio();
        appManager.UIManager.ActivateGameEndMenu();
    }
}