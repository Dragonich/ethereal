﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the lights on the submarine.
/// </summary>
public class LightsManager : MonoBehaviour
{
    [Tooltip("The speed at which the light object can be rotated.")]
    public float RotationSpeed;

    [Tooltip("The GameObject which has the lights object as a child, to rotate the lights object from the appropriate origin.")]
    public GameObject LightsRotator;
    
    /// <summary>
    /// A Vector3 reserved for assigning a new rotation to the lights object.
    /// </summary>
    private Vector3 lightsRotationSetter;

    public void Initialise()
    {
        AssertInspectorInputs();
    }

    private void AssertInspectorInputs()
    {
        Debug.Assert(LightsRotator != null);

        Debug.Assert(!Mathf.Approximately(RotationSpeed, 0f));
    }

    /// <summary>
    /// Rotates the submarine's light object.
    /// </summary>
    /// <param name="rotateClockwise">Whether the light object should be rotated clockwise.</param>
    public void RotateLight(bool rotateClockwise)
    {
        Debug.Assert(LightsRotator != null);

        lightsRotationSetter = LightsRotator.transform.localEulerAngles;

        lightsRotationSetter.z += Time.deltaTime * (rotateClockwise ? RotationSpeed : -RotationSpeed);

        LightsRotator.transform.localEulerAngles = lightsRotationSetter;
    }
}