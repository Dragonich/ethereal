﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the graphic for the submarine's exterior.
/// </summary>
public class SubmarinePanelManager : MonoBehaviour
{
    [Tooltip("The GameObject for the submarine's exterior graphic.")]
    public GameObject PanelObject;

    public void Initialise()
    {
        AssertInspectorInputs();
    }

    private void AssertInspectorInputs()
    {
        Debug.Assert(PanelObject != null);
    }

    public void SetPanelActive(bool newActive)
    {
        Debug.Assert(PanelObject != null);

        PanelObject.SetActive(newActive);
    }
}