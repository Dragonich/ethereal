﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the various UI elements in the game.
/// </summary>
public class UIManager : MonoBehaviour
{
    [Tooltip("The parent GameObject of the Game End menu.")]
    public GameObject GameEndMenu;

    [Tooltip("The parent GameObject of the Fail State menu.")]
    public GameObject FailStateMenu;

    public void Initialise()
    {
        AssertInspectorInputs();

        DisableAllMenus();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GameEndMenu != null);
        Debug.Assert(FailStateMenu != null);
    }

    /// <summary>
    /// Sets all menu objects to be inactive.
    /// </summary>
    private void DisableAllMenus()
    {
        Debug.Assert(GameEndMenu != null);
        Debug.Assert(FailStateMenu != null);

        GameEndMenu.SetActive(false);
        FailStateMenu.SetActive(false);
    }

    public void ActivateFailStateMenu()
    {
        Debug.Assert(FailStateMenu != null);

        FailStateMenu.SetActive(true);
    }

    public void ActivateGameEndMenu()
    {
        Debug.Assert(GameEndMenu != null);

        GameEndMenu.SetActive(true);
    }
}