﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the Fail State menu.
/// </summary>
public class UI_FailStateMenu : MonoBehaviour
{
    /// <summary>
    /// Sets the Time Scale to 1, and reloads the current Scene.
    /// This should be called when the user has clicked the button to Retry.
    /// </summary>
    public void ButtonClickedRetry()
    {
        Time.timeScale = 1f;

        string currentSceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name; // Cache the name of the current Scene.
        UnityEngine.SceneManagement.SceneManager.LoadScene(currentSceneName); // Reload the current Scene.
    }

    /// <summary>
    /// Quits the application.
    /// This should be called when the user has clicked the button to quit.
    /// </summary>
    public void ButtonClickedQuit()
    {
        Application.Quit();
    }
}