﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the Seamonster in the game.
/// </summary>
public class SeamonsterManager : MonoBehaviour
{
    private StateManager stateManager;
    private EncountersManager encountersManager;

    [Tooltip("The Seamonster's maximum number of hit-points.")]
    public int SeamonsterHitPoints;

    /// <summary>
    /// The Seamonster's current number of hit-points.
    /// </summary>
    private int currentSeamonsterHitPoints;

    [Tooltip("The speed at which the Seamonster should move during the first encounter.")]
    public float Encounter1Speed;

    [Tooltip("The speed at which the Seamonster should move during the third encounter.")]
    public float Encounter3Speed;

    public GameObject SeamonsterObject;

    /// <summary>
    /// Cached reference to the Seamonster's Rigidbody component.
    /// </summary>
    private Rigidbody2D rigidbodyComponent;

    /// <summary>
    /// Cached reference to the Seamonster's trigger script.
    /// </summary>
    private SeamonsterTrigger seamonsterTrigger;

    /// <summary>
    /// A Vector2 reserved for assigning a new position to the Seamonster.
    /// </summary>
    private Vector2 seamonsterPositionSetter;

    public void Initialise(StateManager inputStateManager, EncountersManager inputEncountersManager)
    {
        Debug.Assert(inputStateManager != null);
        Debug.Assert(inputEncountersManager != null);

        stateManager = inputStateManager;
        encountersManager = inputEncountersManager;

        AssertInspectorInputs();

        CacheSubordinates();

        seamonsterTrigger.Initialise(this);

        SetActive(false); // Set the Seamonster object to be not active.
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(Encounter1Speed > 0f);
        Debug.Assert(Encounter3Speed > 0f);

        Debug.Assert(SeamonsterHitPoints > 0);

        Debug.Assert(SeamonsterObject != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(SeamonsterObject != null);

        rigidbodyComponent = SeamonsterObject.GetComponent<Rigidbody2D>();
        Debug.Assert(rigidbodyComponent != null); // Assert that the component was found.

        seamonsterTrigger = SeamonsterObject.GetComponent<SeamonsterTrigger>();
        Debug.Assert(seamonsterTrigger != null); // Assert that the component was found.
    }

    /// <summary>
    /// Resets the Seamonster's current hit-points to the maximum.
    /// </summary>
    public void ResetHitPoints()
    {
        currentSeamonsterHitPoints = SeamonsterHitPoints;
    }

    /// <summary>
    /// Returns the position of the Seamonster object.
    /// </summary>
    public Vector3 GetPosition()
    {
        Debug.Assert(SeamonsterObject != null);

        return SeamonsterObject.transform.position;
    }

    /// <summary>
    /// Sets the X and Y position values of the Seamonster.
    /// </summary>
    public void SetPosition(float newPositionX, float newPositionY)
    {
        Debug.Assert(SeamonsterObject != null);

        seamonsterPositionSetter.x = newPositionX;
        seamonsterPositionSetter.y = newPositionY;

        SeamonsterObject.transform.position = seamonsterPositionSetter;
    }

    /// <summary>
    /// Sets the Y position value of the Seamonster,
    /// where the X position value is left at its current value.
    /// </summary>
    public void SetPositionY(float newPositionY)
    {
        Debug.Assert(SeamonsterObject != null);

        seamonsterPositionSetter.x = SeamonsterObject.transform.position.x;
        seamonsterPositionSetter.y = newPositionY;

        SeamonsterObject.transform.position = seamonsterPositionSetter;
    }

    /// <summary>
    /// Flips the Seamonster's horizontal rotation & velocity.
    /// </summary>
    public void FlipSeamonster()
    {
        //Debug.Log("Flipping seamonster.");

        Debug.Assert(SeamonsterObject != null);

        Vector3 eulerAngles = SeamonsterObject.transform.eulerAngles;
        eulerAngles.y += 180f;

        SeamonsterObject.transform.eulerAngles = eulerAngles;

        ResetToRightwardVelocity(true);
    }

    /// <summary>
    /// Sets the rotation of the Seamonster to be horizontal.
    /// </summary>
    public void SetHorizontal()
    {
        Debug.Assert(SeamonsterObject != null);

        SeamonsterObject.transform.eulerAngles = Vector3.zero;
    }

    /// <summary>
    /// Resets the Rigidbody's velocity to be moving right.
    /// </summary>
    /// <param name="encounter1">Whether this is the first encounter, which affects the Seamonster's speed.</param>
    public void ResetToRightwardVelocity(bool encounter1)
    {
        Debug.Assert(SeamonsterObject != null);
        Debug.Assert(rigidbodyComponent != null);

        rigidbodyComponent.velocity = Vector2.zero;
        rigidbodyComponent.AddForce(SeamonsterObject.transform.right * (encounter1 ? Encounter1Speed : Encounter3Speed));
    }

    /// <summary>
    /// Adds vertical force to the Seamonster object's Rigidbody component.
    /// </summary>
    /// <param name="upwardForce">Whether the added force should push the Seamonster upward.</param>
    public void AddVerticalForce(bool upwardForce)
    {
        Debug.Assert(SeamonsterObject != null);
        Debug.Assert(rigidbodyComponent != null);

        Vector3 forceDirection = upwardForce ? SeamonsterObject.transform.up : -SeamonsterObject.transform.up;

        rigidbodyComponent.AddForce(forceDirection * Encounter1Speed);
    }

    /// <summary>
    /// Sets the Seamonster object's active status to the given boolean.
    /// </summary>
    /// <param name="newActive">Whether the Seamonster object should be active.</param>
    public void SetActive(bool newActive)
    {
        Debug.Assert(SeamonsterObject != null);

        SeamonsterObject.SetActive(newActive);
    }

    /// <summary>
    /// Decrements the current hit-points of the Seamonster,
    /// and triggers the encounter to finish if it has no hit-points remaining.
    /// </summary>
    public void SeamonsterProjectileCollision()
    {
        Debug.Assert(encountersManager != null);

        currentSeamonsterHitPoints--;

        if(currentSeamonsterHitPoints == 0) // The Seamonster has been killed.
        {
            encountersManager.TriggerCurrentEncounterToFinish();
        }
    }

    /// <summary>
    /// Trigger the functionality for when the Seamonster has attacked the submarine.
    /// </summary>
    public void TriggerSubmarineAttacked()
    {
        Debug.Assert(stateManager != null);
        Debug.Assert(encountersManager != null);

        switch(encountersManager.GetCurrentEncounter())
        {
            case EncountersManager.Encounters.Encounter1:

                stateManager.TriggerFailState();
                break;

            case EncountersManager.Encounters.Encounter3:

                stateManager.TriggerGameEnd();
                break;

            default:

                Debug.LogError(encountersManager.GetCurrentEncounter());
                break;
        }
    }
}