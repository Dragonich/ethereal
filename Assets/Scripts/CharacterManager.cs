﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the player's character object.
/// </summary>
public class CharacterManager : MonoBehaviour
{
    [Tooltip("The speed at which the character should be moved by the player.")]
    public float CharacterMovementSpeed;

    [Tooltip("The width of the space in which the character can move, to quickly achieve a boundary within the submarine.")]
    public float CharacterMovementWidth;

    [Tooltip("The GameObject which contains the character sprite graphics as children.")]
    public GameObject CharacterObject;

    [Tooltip("The GameObject which has the character's idle sprite animation as a component.")]
    public GameObject IdleSprite;

    [Tooltip("The GameObject which has the character's walk sprite animation as a component.")]
    public GameObject WalkSprite;

    /// <summary>
    /// A cached reference to the character's idle sprite renderer component.
    /// </summary>
    private SpriteRenderer idleSpriteRenderer;

    /// <summary>
    /// A cached reference to the character's walk sprite renderer component.
    /// </summary>
    private SpriteRenderer walkSpriteRenderer;

    /// <summary>
    /// A Vector3 reserved for assigning a new position to the character object.
    /// </summary>
    private Vector3 characterPositionSetter;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(IdleSprite != null);
        Debug.Assert(WalkSprite != null);

        Debug.Assert(CharacterMovementSpeed > 0f);
        Debug.Assert(CharacterMovementWidth > 0f);

        Debug.Assert(CharacterObject != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(IdleSprite != null);
        Debug.Assert(WalkSprite != null);

        idleSpriteRenderer = IdleSprite.GetComponent<SpriteRenderer>();
        Debug.Assert(idleSpriteRenderer != null); // Assert that the component was found.

        walkSpriteRenderer = WalkSprite.GetComponent<SpriteRenderer>();
        Debug.Assert(walkSpriteRenderer != null); // Assert that the component was found.
    }

    /// <summary>
    /// Returns the position of the character object.
    /// </summary>
    public Vector2 GetCharacterPosition()
    {
        Debug.Assert(CharacterObject != null);

        return CharacterObject.transform.position;
    }

    /// <summary>
    /// Moves the character object horizontally.
    /// </summary>
    /// <param name="moveRightward">Whether the character should be moved rightward.</param>
    public void MoveCharacter(bool moveRightward)
    {
        Debug.Assert(CharacterObject != null);
        Debug.Assert(idleSpriteRenderer != null);
        Debug.Assert(walkSpriteRenderer != null);

        ActivateWalkSprite();

        characterPositionSetter = CharacterObject.transform.localPosition; // Cache the character object's local position.

        characterPositionSetter.x += Time.deltaTime * (moveRightward ? CharacterMovementSpeed : -CharacterMovementSpeed);

        if(Mathf.Abs(characterPositionSetter.x) < CharacterMovementWidth) // The new X position is within the submarine movement boundary.
        {
            CharacterObject.transform.localPosition = characterPositionSetter; // Assign the new position to the character object's local position.

            idleSpriteRenderer.flipX = !moveRightward; // Sets the Idle Sprite to be flipped if the character has been moved leftward.
            walkSpriteRenderer.flipX = !moveRightward; // Sets the Walk Sprite to be flipped if the character has been moved leftward.
        }
    }

    /// <summary>
    /// Sets the Idle Sprite to be active,
    /// and sets the Walk Sprite to be inactive.
    /// </summary>
    public void ActivateIdleSprite()
    {
        Debug.Assert(IdleSprite != null);
        Debug.Assert(WalkSprite != null);

        IdleSprite.SetActive(true);
        WalkSprite.SetActive(false);
    }

    /// <summary>
    /// Sets the Walk Sprite to be active,
    /// and sets the Idle Sprite to be inactive.
    /// </summary>
    public void ActivateWalkSprite()
    {
        Debug.Assert(IdleSprite != null);
        Debug.Assert(WalkSprite != null);

        IdleSprite.SetActive(false);
        WalkSprite.SetActive(true);
    }
}