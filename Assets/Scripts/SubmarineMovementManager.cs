﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the submarine's movements.
/// </summary>
public class SubmarineMovementManager : MonoBehaviour
{
    /// <summary>
    /// The directions in which the submarine can be moved.
    /// </summary>
    public enum MovementDirections
    {
        Left,
        Right,
        Up,
        Down
    }

    [Tooltip("The multiplier applied to the force added to the submarine Rigidbody when being moved.")]
    public float MovementMultiplier;

    [Tooltip("The drag which the submarine should experience when its movement is being terminated.")]
    public float TerminationLinearDrag;

    public GameObject SubmarineObject;

    /// <summary>
    /// Cached reference to the submarine object's Rigidbody component.
    /// </summary>
    private Rigidbody2D rigidBodyComponent;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(MovementMultiplier > 0f);
        Debug.Assert(TerminationLinearDrag > 0f);

        Debug.Assert(SubmarineObject != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(SubmarineObject != null);

        rigidBodyComponent = SubmarineObject.GetComponent<Rigidbody2D>();
        Debug.Assert(rigidBodyComponent != null); // Assert that the component was found.
    }

    /// <summary>
    /// Applies force to the submarine's Rigidbody to move it in the given direction.
    /// </summary>
    /// <param name="movementDirection">The direction in which the submarine should be moved.</param>
    public void ApplyMovement(MovementDirections movementDirection)
    {
        Debug.Assert(SubmarineObject != null);
        Debug.Assert(rigidBodyComponent != null);

        Vector3 forceDirection = Vector3.zero;

        switch(movementDirection)
        {
            case MovementDirections.Left:

                forceDirection = -SubmarineObject.transform.right;
                break;

            case MovementDirections.Right:

                forceDirection = SubmarineObject.transform.right;
                break;

            case MovementDirections.Up:

                forceDirection = SubmarineObject.transform.up;
                break;

            case MovementDirections.Down:

                forceDirection = -SubmarineObject.transform.up;
                break;

            default:

                Debug.Assert(false, movementDirection);
                break;
        }

        rigidBodyComponent.AddForce(forceDirection * MovementMultiplier * Time.deltaTime);
    }

    /// <summary>
    /// Returns whether the magnitude of the submarine's velocity is above 0.1f.
    /// </summary>
    public bool GetIsMoving()
    {
        Debug.Assert(rigidBodyComponent != null);

        return Mathf.Abs(rigidBodyComponent.velocity.magnitude) > 0.1f;
    }

    /// <summary>
    /// Sets whether the submarine will experience linear drag and gravity.
    /// 
    /// If terminating forces are active, the submarine will experience linear drag,
    /// but not gravity. If the forces are not active, the submarine will experience
    /// gravity, but not linear drag.
    /// 
    /// This is used to stop the submarine's movement at the start of an Encounter,
    /// and to enable its movement again at the end of the Encounter.
    /// </summary>
    /// <param name="newTerminatingForcesActive">The new status of whether the submarine should experience linear drag but not gravity.</param>
    public void SetTerminatingForcesActive(bool newTerminatingForcesActive)
    {
        Debug.Assert(rigidBodyComponent != null);

        rigidBodyComponent.gravityScale = newTerminatingForcesActive ? 0f : 1f;

        rigidBodyComponent.drag = newTerminatingForcesActive ? TerminationLinearDrag : 0f;
    }

    /// <summary>
    /// Returns the position of the submarine object.
    /// </summary>
    public Vector2 GetPosition()
    {
        Debug.Assert(SubmarineObject != null);

        return SubmarineObject.transform.position;
    }
}