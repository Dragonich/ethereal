﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the user's inputs for the game.
/// </summary>
public class InputManager : MonoBehaviour
{
    private AppManager appManager;

    /// <summary>
    /// Represents whether the character object has been moved this frame.
    /// </summary>
    private bool characterMovedThisFrame;

    public void Initialise(AppManager inputAppManager)
    {
        Debug.Assert(inputAppManager != null);
        appManager = inputAppManager;
    }

    private void Update()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        characterMovedThisFrame = false;

        CheckInputs();

        if(!characterMovedThisFrame)
        {
            appManager.EntitiesManager.ActivateCharacterIdleSprite();
        }
    }

    private void CheckInputs()
    {
        if(Input.GetKey(KeyCode.W)) // The user is holding down the W key.
        {
            ApplyW();
        }
        else if(Input.GetKey(KeyCode.A)) // The user is holding down the A key.
        {
            ApplyA();
        }
        else if(Input.GetKey(KeyCode.S)) // The user is holding down the S key.
        {
            ApplyS();
        }
        else if(Input.GetKey(KeyCode.D)) // The user is holding down the D key.
        {
            ApplyD();
        }
        
        if(Input.GetKeyDown(KeyCode.Q)) // The user has pressed the Q key this frame.
        {
            ApplyQ();
        }
        
        if(Input.GetKey(KeyCode.LeftArrow)) // The user is holding down the Left Arrow key.
        {
            ApplyLeftArrow();
        }
        else if(Input.GetKey(KeyCode.RightArrow)) // The user is holding down the Right Arrow Key.
        {
            ApplyRightArrow();
        }

        if(Input.GetKeyDown(KeyCode.Space)) // The user has pressed the Space key this frame.
        {
            ApplySpace();
        }
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the W key.
    /// </summary>
    private void ApplyW()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        if(appManager.StateManager.CurrentState == StateManager.States.Pilot)
        {
            appManager.EntitiesManager.MoveSubmarine(SubmarineMovementManager.MovementDirections.Up);
        }
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the A key.
    /// </summary>
    private void ApplyA()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        switch(appManager.StateManager.CurrentState)
        {
            case StateManager.States.Character:

                characterMovedThisFrame = true;
                appManager.EntitiesManager.MoveCharacter(false);
                break;

            case StateManager.States.Pilot:

                appManager.EntitiesManager.MoveSubmarine(SubmarineMovementManager.MovementDirections.Left);
                break;
        }
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the S key.
    /// </summary>
    private void ApplyS()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        if (appManager.StateManager.CurrentState == StateManager.States.Pilot)
        {
            appManager.EntitiesManager.MoveSubmarine(SubmarineMovementManager.MovementDirections.Down);
        }
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the D key.
    /// </summary>
    private void ApplyD()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        switch (appManager.StateManager.CurrentState)
        {
            case StateManager.States.Character:

                characterMovedThisFrame = true;
                appManager.EntitiesManager.MoveCharacter(true);
                break;

            case StateManager.States.Pilot:

                appManager.EntitiesManager.MoveSubmarine(SubmarineMovementManager.MovementDirections.Right);
                break;
        }
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the Q key.
    /// </summary>
    private void ApplyQ()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);

        appManager.StateManager.ActivateState(appManager.StateManager.CurrentState == StateManager.States.Character);
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the Left Arrow key.
    /// </summary>
    private void ApplyLeftArrow()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        switch (appManager.StateManager.CurrentState)
        {
            case StateManager.States.Lights:

                appManager.EntitiesManager.RotateSubmarineLight(false);
                break;

            case StateManager.States.Weapons:

                appManager.EntitiesManager.RotateSubmarineTurret(false);
                break;
        }
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the Right Arrow key.
    /// </summary>
    private void ApplyRightArrow()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        switch (appManager.StateManager.CurrentState)
        {
            case StateManager.States.Lights:

                appManager.EntitiesManager.RotateSubmarineLight(true);
                break;

            case StateManager.States.Weapons:

                appManager.EntitiesManager.RotateSubmarineTurret(true);
                break;
        }
    }

    /// <summary>
    /// Applies the functionality which should be triggered by the Space key.
    /// </summary>
    private void ApplySpace()
    {
        Debug.Assert(appManager != null);
        Debug.Assert(appManager.StateManager != null);
        Debug.Assert(appManager.EntitiesManager != null);

        if (appManager.StateManager.CurrentState == StateManager.States.Weapons)
        {
            appManager.EntitiesManager.FireTurretProjectile();
        }
    }
}