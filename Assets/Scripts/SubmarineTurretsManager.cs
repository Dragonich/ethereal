﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the turrets on the submarine.
/// </summary>
public class SubmarineTurretsManager : MonoBehaviour
{
    [Tooltip("The speed at which the turret should be rotated by the player.")]
    public float RotationSpeed;

    public GameObject TurretObject;

    /// <summary>
    /// A Vector3 reserved for assigning a new rotation to the turret object.
    /// </summary>
    private Vector3 turretRotationSetter;

    [Tooltip("The amount of force which is applied to a projectile upon being fired.")]
    public float ProjectileForce;

    [Tooltip("The prefab for the projectile which is fired from the turret.")]
    public GameObject ProjectilePrefab;

    public void Initialise()
    {
        AssertInspectorInputs();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(RotationSpeed > 0f);
        Debug.Assert(TurretObject != null);

        Debug.Assert(ProjectileForce > 0f);
        Debug.Assert(ProjectilePrefab != null);
    }

    /// <summary>
    /// Sets the submarine turret object visible.
    /// </summary>
    /// <param name="newVisible">Whether the turret object should be visible.</param>
    public void SetTurretVisible(bool newVisible)
    {
        Debug.Assert(TurretObject != null);

        TurretObject.SetActive(newVisible);
    }

    /// <summary>
    /// Rotates the submarine's turret object.
    /// </summary>
    /// <param name="rotateClockwise">Whether the turret object should be rotated clockwise.</param>
    public void RotateTurret(bool rotateClockwise)
    {
        Debug.Assert(TurretObject != null);

        turretRotationSetter = TurretObject.transform.localEulerAngles;

        turretRotationSetter.z += Time.deltaTime * (rotateClockwise ? RotationSpeed : -RotationSpeed);

        TurretObject.transform.localEulerAngles = turretRotationSetter;
    }

    /// <summary>
    /// Instantiates a new projectile object and fires it from the submarine turret.
    /// </summary>
    public void FireProjectile()
    {
        Debug.Assert(ProjectilePrefab != null);

        GameObject newProjectile = Instantiate(ProjectilePrefab); // Instantiate a new projectile object to be fired.

        newProjectile.transform.position = TurretObject.transform.position; // Align the projectile's position to the turret.
        newProjectile.transform.eulerAngles = TurretObject.transform.localEulerAngles; // Align the projectile's rotation to the turret.

        Rigidbody2D rigidBodyComponent = newProjectile.GetComponent<Rigidbody2D>();
        Debug.Assert(rigidBodyComponent != null); // Assert that the component was found.

        rigidBodyComponent.AddForce(newProjectile.transform.right * ProjectileForce);
    }
}