﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the submarine in the game.
/// </summary>
public class SubmarineManager : MonoBehaviour
{
    [Tooltip("The GameObject which holds the Lights Manager as a component.")]
    public GameObject GO_LightsManager;
    private LightsManager lightsManager;

    [Tooltip("The GameObject which holds the Submarine Panel Manager as a component.")]
    public GameObject GO_SubmarinePanelManager;
    private SubmarinePanelManager submarinePanelManager;

    [Tooltip("The GameObject which holds the Submarine Turrets Manager as a component.")]
    public GameObject GO_SubmarineTurretsManager;
    private SubmarineTurretsManager submarineTurretsManager;

    [Tooltip("The GameObject which holds the Submarine Movement Manager as a component.")]
    public GameObject GO_SubmarineMovementManager;
    private SubmarineMovementManager submarineMovementManager;

    [Tooltip("The GameObject which holds the Submarine Terminals Manager as a component.")]
    public GameObject GO_SubmarineTerminalsManager;
    private SubmarineTerminalsManager submarineTerminalsManager;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();

        InitialiseSubordinates();

        SetSubmarineInteriorVisible(true);
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_LightsManager != null);
        Debug.Assert(GO_SubmarinePanelManager != null);
        Debug.Assert(GO_SubmarineTurretsManager != null);
        Debug.Assert(GO_SubmarineMovementManager != null);
        Debug.Assert(GO_SubmarineTerminalsManager != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_LightsManager != null);
        Debug.Assert(GO_SubmarinePanelManager != null);
        Debug.Assert(GO_SubmarineTurretsManager != null);
        Debug.Assert(GO_SubmarineMovementManager != null);
        Debug.Assert(GO_SubmarineTerminalsManager != null);

        lightsManager = GO_LightsManager.GetComponent<LightsManager>();
        Debug.Assert(lightsManager != null); // Assert that the component was found.

        submarinePanelManager = GO_SubmarinePanelManager.GetComponent<SubmarinePanelManager>();
        Debug.Assert(submarinePanelManager != null); // Assert that the component was found.

        submarineTurretsManager = GO_SubmarineTurretsManager.GetComponent<SubmarineTurretsManager>();
        Debug.Assert(submarineTurretsManager != null); // Assert that the component was found.

        submarineMovementManager = GO_SubmarineMovementManager.GetComponent<SubmarineMovementManager>();
        Debug.Assert(submarineMovementManager != null); // Assert that the component was found.

        submarineTerminalsManager = GO_SubmarineTerminalsManager.GetComponent<SubmarineTerminalsManager>();
        Debug.Assert(submarineTerminalsManager != null); // Assert that the component was found.
    }

    private void InitialiseSubordinates()
    {
        Debug.Assert(lightsManager != null);
        Debug.Assert(submarinePanelManager != null);
        Debug.Assert(submarineTurretsManager != null);
        Debug.Assert(submarineMovementManager != null);
        Debug.Assert(submarineTerminalsManager != null);

        lightsManager.Initialise();

        submarinePanelManager.Initialise();

        submarineTurretsManager.Initialise();

        submarineMovementManager.Initialise();

        submarineTerminalsManager.Initialise();
    }

    /// <summary>
    /// Sets the submarine's exterior panel and turret visible
    /// depending on whether the submarine's interior should be visible.
    /// </summary>
    /// <param name="newInteriorVisible">Whether the submarine's interior should be visible.</param>
    public void SetSubmarineInteriorVisible(bool newInteriorVisible)
    {
        Debug.Assert(submarinePanelManager != null);
        Debug.Assert(submarineTurretsManager != null);

        submarinePanelManager.SetPanelActive(!newInteriorVisible); // Sets the submarine's exterior panel visible when the submarine interior is not visible.

        submarineTurretsManager.SetTurretVisible(!newInteriorVisible); // Sets the submarine's turret visible when the submarine interior is not visible.
    }

    /// <summary>
    /// Returns the enum value of the closest valid terminal.
    /// If no valid terminal is found, returns the enum value of None.
    /// </summary>
    /// <param name="characterPosition">The position of the character object to be compared against terminal object positions.</param>
    public SubmarineTerminalsManager.Terminals GetClosestValidTerminal(Vector2 characterPosition)
    {
        Debug.Assert(submarineTerminalsManager != null);

        return submarineTerminalsManager.GetClosesetValidTerminal(characterPosition);
    }

    /// <summary>
    /// Rotates the submarine's light object.
    /// </summary>
    /// <param name="rotateClockwise">Whether the light object should be rotated clockwise.</param>
    public void RotateLight(bool rotateClockwise)
    {
        Debug.Assert(lightsManager != null);

        lightsManager.RotateLight(rotateClockwise);
    }

    /// <summary>
    /// Rotates the submarine's turret object.
    /// </summary>
    /// <param name="rotateClockwise">Whether the turret object should be rotated clockwise.</param>
    public void RotateTurret(bool rotateClockwise)
    {
        Debug.Assert(submarineTurretsManager != null);

        submarineTurretsManager.RotateTurret(rotateClockwise);
    }

    /// <summary>
    /// Applies force to the submarine's Rigidbody to move it in the given direction.
    /// </summary>
    /// <param name="movementDirection">The direction in which the submarine should be moved.</param>
    public void MoveSubmarine(SubmarineMovementManager.MovementDirections movementDirection)
    {
        Debug.Assert(submarineMovementManager != null);

        submarineMovementManager.ApplyMovement(movementDirection);
    }

    /// <summary>
    /// Returns the position of the submarine object.
    /// </summary>
    public Vector2 GetPosition()
    {
        Debug.Assert(submarineMovementManager != null);

        return submarineMovementManager.GetPosition();
    }

    /// <summary>
    /// Instantiates a new projectile object and fires it from the submarine turret.
    /// </summary>
    public void FireProjectile()
    {
        Debug.Assert(submarineTurretsManager != null);

        submarineTurretsManager.FireProjectile();
    }

    /// <summary>
    /// Returns whether the magnitude of the submarine's velocity is above 0.1f.
    /// </summary>
    public bool GetIsMoving()
    {
        Debug.Assert(submarineMovementManager != null);

        return submarineMovementManager.GetIsMoving();
    }

    /// <summary>
    /// Sets whether the submarine will experience linear drag and gravity.
    /// 
    /// If terminating forces are active, the submarine will experience linear drag,
    /// but not gravity. If the forces are not active, the submarine will experience
    /// gravity, but not linear drag.
    /// 
    /// This is used to stop the submarine's movement at the start of an Encounter,
    /// and to enable its movement again at the end of the Encounter.
    /// </summary>
    /// <param name="newTerminatingForcesActive">The new status of whether the submarine should experience linear drag but not gravity.</param>
    public void SetTerminatingForcesActive(bool newTerminatingForcesActive)
    {
        Debug.Assert(submarineMovementManager != null);

        submarineMovementManager.SetTerminatingForcesActive(newTerminatingForcesActive);
    }
}