﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the top-most references in the app.
/// </summary>
public class AppManager : MonoBehaviour
{
    [Tooltip("The GameObject which holds the UI Manager as a component.")]
    public GameObject GO_UIManager;

    [Tooltip("The GameObject which holds the Audio Manager as a component.")]
    public GameObject GO_AudioManager;

    [Tooltip("The GameObject which holds the State Manager as a component.")]
    public GameObject GO_StateManager;

    [Tooltip("The GameObject which holds the Input Manager as a component.")]
    public GameObject GO_InputManager;

    [Tooltip("The GameObject which holds the Camera Manager as a component.")]
    public GameObject GO_CameraManager;

    [Tooltip("The GameObject which holds the Entities Manager as a component.")]
    public GameObject GO_EntitiesManager;

    [Tooltip("The GameObject which holds the Encounters Manager as a component.")]
    public GameObject GO_EncountersManager;

    public UIManager UIManager { get; private set; }
    public AudioManager AudioManager { get; private set; }
    public StateManager StateManager { get; private set; }
    public InputManager InputManager { get; private set; }
    public CameraManager CameraManager { get; private set; }
    public EntitiesManager EntitiesManager { get; private set; }
    public EncountersManager EncountersManager { get; private set; }

    public void Awake()
    {
        AssertInspectorInputs();

        CacheSubordinates();

        InitialiseSubordinates();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_UIManager != null);
        Debug.Assert(GO_AudioManager != null);
        Debug.Assert(GO_StateManager != null);
        Debug.Assert(GO_InputManager != null);
        Debug.Assert(GO_CameraManager != null);
        Debug.Assert(GO_EntitiesManager != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_UIManager != null);
        Debug.Assert(GO_AudioManager != null);
        Debug.Assert(GO_StateManager != null);
        Debug.Assert(GO_InputManager != null);
        Debug.Assert(GO_CameraManager != null);
        Debug.Assert(GO_EntitiesManager != null);
        Debug.Assert(GO_EncountersManager != null);

        UIManager = GO_UIManager.GetComponent<UIManager>();
        Debug.Assert(UIManager != null); // Assert that the component was found.

        AudioManager = GO_AudioManager.GetComponent<AudioManager>();
        Debug.Assert(AudioManager != null); // Assert that the component was found.

        StateManager = GO_StateManager.GetComponent<StateManager>();
        Debug.Assert(StateManager != null); // Assert that the component was found.

        InputManager = GO_InputManager.GetComponent<InputManager>();
        Debug.Assert(InputManager != null); // Assert that the component was found.

        CameraManager = GO_CameraManager.GetComponent<CameraManager>();
        Debug.Assert(CameraManager != null); // Assert that the component was found.

        EntitiesManager = GO_EntitiesManager.GetComponent<EntitiesManager>();
        Debug.Assert(EntitiesManager != null); // Assert that the component was found.

        EncountersManager = GO_EncountersManager.GetComponent<EncountersManager>();
        Debug.Assert(EncountersManager != null); // Assert that the component was found.
    }

    private void InitialiseSubordinates()
    {
        Debug.Assert(UIManager != null);
        Debug.Assert(AudioManager != null);
        Debug.Assert(CameraManager != null);
        Debug.Assert(InputManager != null);
        Debug.Assert(StateManager != null);
        Debug.Assert(EntitiesManager != null);
        Debug.Assert(EncountersManager != null);

        UIManager.Initialise();
        AudioManager.Initialise();
        CameraManager.Initialise();
        InputManager.Initialise(this);
        StateManager.Initialise(this);
        EntitiesManager.Initialise(this);
        EncountersManager.Initialise(this);
    }
}