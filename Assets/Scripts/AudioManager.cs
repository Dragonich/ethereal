﻿// AUTHOR: Benjamin Chaves

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages the various audio clips in the game.
/// </summary>
public class AudioManager : MonoBehaviour
{
    [Tooltip("The GameObject which contains the sonar audio source as a component.")]
    public GameObject GO_SonarAudio;

    [Tooltip("The GameObject which contains the music audio source as a component.")]
    public GameObject GO_MusicAudio;

    [Tooltip("The GameObject which contains the groan audio source as a component.")]
    public GameObject GO_GroanAudio;

    [Tooltip("The GameObject which contains the growl audio source as a component.")]
    public GameObject GO_GrowlAudio;

    [Tooltip("The GameObject which contains the fail state audio source as a component.")]
    public GameObject GO_FailStateAudio;

    [Tooltip("The GameObject which contains the underwater audio source as a component.")]
    public GameObject GO_UnderwaterAudio;

    /// <summary>
    /// Cached reference to the sonar audio source component.
    /// </summary>
    private AudioSource sonarAudio;

    /// <summary>
    /// Cached reference to the music audio source component.
    /// </summary>
    private AudioSource musicAudio;

    /// <summary>
    /// Cached reference to the groan audio source component.
    /// </summary>
    private AudioSource groanAudio;

    /// <summary>
    /// Cached reference to the growl audio source component.
    /// </summary>
    private AudioSource growlAudio;

    /// <summary>
    /// Cached reference to the fail state audio source component.
    /// </summary>
    private AudioSource failStateAudio;

    /// <summary>
    /// Cached reference to the underwater audio source component.
    /// </summary>
    private AudioSource underwaterAudio;

    public void Initialise()
    {
        AssertInspectorInputs();

        CacheSubordinates();
    }

    /// <summary>
    /// Asserts that all Inspector fields have been set or filled.
    /// </summary>
    private void AssertInspectorInputs()
    {
        Debug.Assert(GO_SonarAudio != null);
        Debug.Assert(GO_MusicAudio != null);
        Debug.Assert(GO_GroanAudio != null);
        Debug.Assert(GO_GrowlAudio != null);
        Debug.Assert(GO_FailStateAudio != null);
        Debug.Assert(GO_UnderwaterAudio != null);
    }

    private void CacheSubordinates()
    {
        Debug.Assert(GO_SonarAudio != null);
        Debug.Assert(GO_MusicAudio != null);
        Debug.Assert(GO_GroanAudio != null);
        Debug.Assert(GO_GrowlAudio != null);
        Debug.Assert(GO_FailStateAudio != null);
        Debug.Assert(GO_UnderwaterAudio != null);

        sonarAudio = GO_SonarAudio.GetComponent<AudioSource>();
        Debug.Assert(sonarAudio != null); // Assert that the component was found.

        musicAudio = GO_MusicAudio.GetComponent<AudioSource>();
        Debug.Assert(musicAudio != null); // Assert that the component was found.

        groanAudio = GO_GroanAudio.GetComponent<AudioSource>();
        Debug.Assert(groanAudio != null); // Assert that the component was found.

        growlAudio = GO_GrowlAudio.GetComponent<AudioSource>();
        Debug.Assert(growlAudio != null); // Assert that the component was found.

        failStateAudio = GO_FailStateAudio.GetComponent<AudioSource>();
        Debug.Assert(failStateAudio != null); // Assert that the component was found.

        underwaterAudio = GO_UnderwaterAudio.GetComponent<AudioSource>();
        Debug.Assert(underwaterAudio != null); // Assert that the component was found.
    }

    /// <summary>
    /// Play the fail state audio once.
    /// </summary>
    public void PlayFailStateAudio()
    {
        Debug.Assert(failStateAudio != null);
        Debug.Assert(failStateAudio.clip != null);

        failStateAudio.PlayOneShot(failStateAudio.clip);
    }

    /// <summary>
    /// Triggers the sonar audio clip to be played on a loop.
    /// </summary>
    public void PlayLoopedSonarAudio()
    {
        Debug.Assert(sonarAudio != null);
        Debug.Assert(sonarAudio.clip != null);

        sonarAudio.Play();
    }

    /// <summary>
    /// Stops the sonar audio clip from being played.
    /// </summary>
    public void StopSonarAudio()
    {
        Debug.Assert(sonarAudio != null);

        sonarAudio.enabled = false;
    }

    /// <summary>
    /// Stops the underwater audio clip from being played.
    /// </summary>
    public void StopUnderwaterAudio()
    {
        Debug.Assert(underwaterAudio != null);

        underwaterAudio.enabled = false;
    }

    /// <summary>
    /// Stops the music audio clip from being played.
    /// </summary>
    public void StopMusicAudio()
    {
        Debug.Assert(musicAudio != null);

        musicAudio.enabled = false;
    }

    /// <summary>
    /// Play the groan audio clip once.
    /// </summary>
    public void PlayGroanAudio()
    {
        Debug.Assert(groanAudio != null);
        Debug.Assert(groanAudio.clip != null);

        groanAudio.PlayOneShot(groanAudio.clip);
    }

    /// <summary>
    /// Play the growl audio clip once.
    /// </summary>
    public void PlayGrowlAudio()
    {
        Debug.Assert(growlAudio != null);
        Debug.Assert(growlAudio.clip != null);

        growlAudio.PlayOneShot(growlAudio.clip);
    }
}